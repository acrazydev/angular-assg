import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class WeatherDataService {

	constructor(private http: HttpClient) { }

	fetchData(): Observable<object> {
		return this.http.get('https://samples.openweathermap.org/data/2.5/forecast/hourly?id=524901&appid=b6907d289e10d714a6e88b30761fae22')
	}
}
