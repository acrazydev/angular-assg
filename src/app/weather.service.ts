import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class WeatherService {

    weather: any
    constructor(private http: HttpClient) { }

    fetchweather(): Observable<any> {
        return this.http.get<any>('/assets/data/Weather.json');
    }

    setcurrentweather(weather) {
        this.weather = weather

    }

    getcurrentweather() {
        return this.weather
    }

}